import 'package:intl/intl.dart';

final orderDateFormat = DateFormat('dd/MM/yyyy HH:mm a');
final orderDateFormat2 = DateFormat('dd/MM/yyyy');
final orderDetailsDateFormat = DateFormat('EEEE, dd MMM, yyyy');
final joiningDateFormat = DateFormat('dd MMM, yyyy');
