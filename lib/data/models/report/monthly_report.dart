import 'package:flutter/material.dart';

class MonthlyReport {
  Color? color;
  String? title;
  dynamic value;
  double? percentage;

  MonthlyReport({
    this.color,
    this.title,
    this.value,
    this.percentage,
  });
}
