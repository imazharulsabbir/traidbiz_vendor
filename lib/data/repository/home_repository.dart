import 'package:flutter/material.dart';
import 'package:traidbiz/data/local/auth_db.dart';
import 'package:traidbiz/data/models/seller_dashboard/seller_dashboard_model.dart';
import 'package:traidbiz/data/remote/api_service.dart';

import '../../constraints/api_endpoints.dart';
import 'package:dio/dio.dart' as dio;

class HomeRepository {
  final _dio = dio.Dio();

  Future<SellerDashboard> getSellerDashboard() async {
    final _response = await ApiService.post(sellerDashboard, _dio, body: {
      "cookie": AuthDb.getAuthCookie()?.cookie,
    });
    debugPrint(_response.toString());
    final _result = SellerDashboard.fromJson(_response);
    // debugPrint(_result.toString());
    return _result;
  }
}
