import 'package:dio/dio.dart' as dio;
import 'dart:convert' as convert;

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

import '../../controller/auth_controller.dart';

const _baseUrl = "https://traidbiz.com";

class ApiService {
  static Future<dynamic> get(
    String url,
    dio.Dio client, {
    Map<String, dynamic>? headers,
  }) async {
    try {
      debugPrint(":: Get $url::");
      client.interceptors.add(PrettyDioLogger(
        requestHeader: false,
        requestBody: false,
        responseBody: false,
        responseHeader: false,
        error: false,
        compact: false,
        maxWidth: 90,
      ));

      final response = await client.get(
        _baseUrl + url,
        options: dio.Options(
          headers: headers,
        ),
      );
      if (response.statusCode == 200) {
        if (response.data?['status'] == 'error') {
          if (response.data?['message']?.contains("generate_auth_cookie")) {
            final AuthController _controller = Get.find<AuthController>();
            _controller.logout();
          }
          return response.data;
        } else {
          return response.data;
        }
      } else {
        return Future.error('Failed to load data');
      }
    } on dio.DioError catch (error) {
      return Future.error(error.message);
    } catch (e) {
      return Future.error(e.toString());
    }
  }

  static Future<dynamic> post(
    String url,
    dio.Dio client, {
    Map<String, dynamic>? headers,
    body,
  }) async {
    try {
      debugPrint(":: Post $url::");
      client.interceptors.add(PrettyDioLogger(
        requestHeader: false,
        requestBody: false,
        responseBody: false,
        responseHeader: false,
        error: false,
        compact: false,
        maxWidth: 90,
      ));

      final response = await client.post(
        _baseUrl + url,
        // options: dio.Options(headers: headers),
        data: body,
      );
      // debugPrint(":: Response $response::");
      if (response.statusCode == 200) {
        if (response.data?['status'] == 'error') {
          if (response.data?['message']?.contains("generate_auth_cookie")) {
            final AuthController _controller = Get.find<AuthController>();
            _controller.logout();
          }
          return response.data;
        } else {
          return response.data;
        }
      } else {
        return Future.error('Failed to load data');
      }
    } on dio.DioError catch (error) {
      return Future.error(error.message);
    } catch (e) {
      return Future.error(e.toString());
    }
  }
}
