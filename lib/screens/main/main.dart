import 'package:flutter/material.dart';
import 'package:flutter_phosphor_icons/flutter_phosphor_icons.dart';
import 'package:flutter_svg/svg.dart';
import 'package:traidbiz/screens/product/products.dart';

import '../home/home.dart';
import '../notification/notification.dart';
import '../order/orders.dart';
import '../settings/settings.dart';
import 'widgets/drawer.dart';
import 'widgets/nav_button_widget.dart';

class MainScreen extends StatefulWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  int _index = 2;
  final List<Widget> _screens = const [
    ProductsScreen(),
    NotificationScreen(),
    HomeScreen(),
    OrderScreen(),
    SettingScreen(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: IndexedStack(
        index: _index,
        children: List.generate(_screens.length, (index) => _screens[index]),
      ),
      resizeToAvoidBottomInset: false,
      bottomNavigationBar: Container(
        decoration: const BoxDecoration(color: Colors.white),
        padding: const EdgeInsets.only(bottom: 10),
        height: kBottomNavigationBarHeight + 30,
        child: Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            BottomNavButtonWidget(
              onTap: () {
                setState(() {
                  _index = 0;
                });
              },
              icon: PhosphorIcons.package,
              text: 'Product',
              isSelected: _index == 0,
            ),
            BottomNavButtonWidget(
              onTap: () {
                setState(() {
                  _index = 1;
                });
              },
              icon: Icons.notifications,
              text: 'Notification',
              isSelected: _index == 1,
            ),
            BottomNavCenterButtonWidget(
              onTap: () {
                setState(() {
                  _index = 2;
                });
              },
              icon: CustomPaint(
                painter: SelectedNavShape(),
                child: const SizedBox(
                  height: 45,
                  width: 40,
                  child: Icon(
                    Icons.dashboard,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
            BottomNavButtonWidget(
              onTap: () {
                setState(() {
                  _index = 3;
                });
              },
              icon: Icons.list_alt,
              text: 'Orders',
              isSelected: _index == 3,
            ),
            BottomNavButtonWidget(
              onTap: () {
                setState(() {
                  _index = 4;
                });
              },
              icon: Icons.settings,
              text: 'Settings',
              isSelected: _index == 4,
            ),
          ],
        ),
      ),
    );
  }
}
