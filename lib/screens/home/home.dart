import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_phosphor_icons/flutter_phosphor_icons.dart';
import 'package:get/get.dart';
import '/controller/home_controller.dart';
import '../../constraints/colors.dart';
import '../../utils/date_formatter.dart';
import '../profile/profile.dart';
import 'widgets/latest_sales_widget.dart';
import 'widgets/report_grid_widget.dart';
import 'widgets/sales_by_week_stacked_chart.dart';

class HomeScreen extends GetView<HomeController> {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double _crossAxisSpacing = 8, _mainAxisSpacing = 12, _aspectRatio = 1.2;
    int _crossAxisCount = 2;

    return controller.obx(
      (state) => Scaffold(
        appBar: AppBar(
          centerTitle: false,
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('Hi! ${controller.authCookie?.user?.displayName}'),
              Text(
                joiningDateFormat.format(DateTime.now()),
                style: Theme.of(context).textTheme.caption,
              ),
            ],
          ),
          leading: const Icon(PhosphorIcons.list_bold),
          actions: [
            GestureDetector(
              onTap: (() => Get.to(() => const ProfileScreen())),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: CircleAvatar(
                  backgroundImage: CachedNetworkImageProvider(
                    '${controller.authCookie?.user?.profileImage}',
                  ),
                ),
              ),
            )
          ],
        ),
        body: RefreshIndicator(
          onRefresh: controller.refreshHomepage,
          child: SingleChildScrollView(
            controller: controller.scroll,
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 18.0)
                      .copyWith(top: 20),
                  child: Row(
                    children: [
                      Text(
                        'This Month',
                        style: Theme.of(context).textTheme.headline6?.copyWith(
                              fontWeight: FontWeight.bold,
                            ),
                      ),
                      // const Spacer(),
                      // TextButton(
                      //     onPressed: () {}, child: const Text('All Report')),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10.0),
                  child: Obx(
                    () => controller.monthlyReportList.isEmpty
                        ? const Center(
                            child: CircularProgressIndicator(),
                          )
                        : GridView.builder(
                            itemCount: controller.monthlyReportList.length,
                            physics: const NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            clipBehavior: Clip.none,
                            itemBuilder: (context, index) => ReportGridWidget(
                              report: controller.monthlyReportList[index],
                            ),
                            gridDelegate:
                                SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: _crossAxisCount,
                              crossAxisSpacing: _crossAxisSpacing,
                              mainAxisSpacing: _mainAxisSpacing,
                              childAspectRatio: _aspectRatio,
                            ),
                          ),
                  ),
                ),
                const SizedBox(height: 20),
                Card(
                  margin: const EdgeInsets.all(0),
                  child: Padding(
                    padding: const EdgeInsets.all(18.0),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Text(
                              'Latest Sales',
                              style: Theme.of(context)
                                  .textTheme
                                  .headline6
                                  ?.copyWith(
                                    fontWeight: FontWeight.bold,
                                  ),
                            ),
                            // const Spacer(),
                            // TextButton(
                            //   onPressed: () {},
                            //   child: const Text(
                            //     'See All',
                            //     style: TextStyle(
                            //         decoration: TextDecoration.underline),
                            //   ),
                            // ),
                          ],
                        ),
                        const SizedBox(height: 20),
                        controller.obx(
                          (state) => LatestSales(state: state),
                          onLoading:
                              const Center(child: CircularProgressIndicator()),
                          onEmpty: const Center(
                            child: Text(
                              'Orders not found',
                              style: TextStyle(fontSize: 18),
                              textAlign: TextAlign.center,
                            ),
                          ),
                          onError: (error) => Center(
                            child: Text(
                              'Error: Cannot get orders \n$error',
                              style: const TextStyle(fontSize: 18),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
