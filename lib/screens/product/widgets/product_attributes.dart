import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '/controller/product/product_detail_controller.dart';
import '../../../constraints/colors.dart';
import '../../../constraints/strings.dart';
import '../../../constraints/styles.dart';
import 'product_attribute.dart';

class ProductAttributesWidget extends GetView<ProductDetailController> {
  const ProductAttributesWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 0,
      child: Padding(
        padding: const EdgeInsets.symmetric(
          vertical: 12.0,
          horizontal: 20,
        ),
        child: Form(
          key: controller.attributeFormKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Attributes',
                style: Theme.of(context)
                    .textTheme
                    .titleLarge
                    ?.copyWith(fontWeight: FontWeight.bold),
              ),
              const SizedBox(height: 8),
              DropdownButtonFormField<String>(
                decoration: inputDecorationFilled.copyWith(
                  isDense: true,
                  hintText: 'Select Attribute',
                ),
                validator: (value) {
                  if (value == null) {
                    return "Please select an attribute type";
                  } else {
                    return null;
                  }
                },
                items: List.generate(
                  productAttributes.length,
                  (index) => DropdownMenuItem(
                    child: Text(productAttributes[index]),
                    value: productAttributes[index],
                  ),
                ),
                onChanged: (value) {
                  controller.selectedAttribute = value;
                },
              ),
              Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 30,
                  vertical: 20,
                ),
                child: SizedBox(
                  width: double.infinity,
                  height: 50,
                  child: ElevatedButton(
                    onPressed: () async {
                      if (controller.attributeFormKey.currentState
                              ?.validate() ==
                          true) {
                        controller.setProductAttribute(
                          ProductAttributeWidget(
                            name: controller.selectedAttribute,
                            isExpanded: true,
                          ),
                        );
                      }
                    },
                    child: const Text('ADD ATTRIBUTES'),
                    style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8),
                      ),
                      primary: colorSecondary,
                    ),
                  ),
                ),
              ),
              const SizedBox(height: 10),
              Obx(
                () => Visibility(
                  visible: controller.productAttributeWidgets.isNotEmpty,
                  child: Container(
                    decoration: BoxDecoration(
                      border: Border.all(color: colorBorder),
                    ),
                    padding: const EdgeInsets.all(8),
                    child: Column(
                      children: List.generate(
                        controller.productAttributeWidgets.length,
                        (index) => controller.productAttributeWidgets[index],
                      )..add(
                          SizedBox(
                            width: double.infinity,
                            height: 50,
                            child: ElevatedButton(
                              onPressed: () async {
                                controller.saveProductAttributes();
                              },
                              child: const Text('SAVE'),
                              style: ElevatedButton.styleFrom(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8),
                                ),
                                primary: colorSecondary,
                              ),
                            ),
                          ),
                        ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
