import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../constraints/colors.dart';
import '../../../controller/category_controller.dart';

class ProductCategoryChipWidget extends GetView<CategoryController> {
  const ProductCategoryChipWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return controller.obx(
      (data) => Scaffold(
        floatingActionButton: FloatingActionButton(
          onPressed: () => Get.back(),
          child: const Icon(
            Icons.arrow_forward_outlined,
          ),
        ),
        body: SingleChildScrollView(
          controller: controller.scroll,
          padding: const EdgeInsets.all(16.0),
          child: Wrap(
            alignment: WrapAlignment.start,
            spacing: 4,
            children: List.generate(
              controller.categories?.length ?? 0,
              (index) => FilterChip(
                label: Text(
                  "${controller.categories?[index].name}",
                  style: TextStyle(
                    color: controller.categories?[index].isSelected ?? false
                        ? Colors.white
                        : Colors.black,
                  ),
                ),
                selected: controller.categories?[index].isSelected ?? false,
                backgroundColor: Colors.white,
                selectedColor: colorPrimary,
                checkmarkColor: Colors.white,
                onSelected: (value) {
                  controller.updateProductSelection(
                    controller.categories?[index],
                  );
                },
              ),
            ),
          ),
        ),
      ),
      onEmpty: const Center(
        child: Text('No Categories available'),
      ),
      onError: (error) => Center(
        child: Text('Failed to load categories. \n$error'),
      ),
    );
  }
}
