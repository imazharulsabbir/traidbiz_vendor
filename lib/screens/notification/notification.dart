import 'package:flutter/material.dart';
import 'package:traidbiz/constraints/colors.dart';

class NotificationScreen extends StatelessWidget {
  const NotificationScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Notification'),
      ),
      body: ListView.separated(
        padding: const EdgeInsets.symmetric(vertical: 20),
        itemBuilder: (context, index) => Container(
          decoration: BoxDecoration(
            color: Colors.white,
            border: Border.symmetric(
              horizontal: BorderSide(
                color: Colors.black.withOpacity(0.1),
              ),
            ),
          ),
          padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 16),
          child: Column(
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const CircleAvatar(child: Icon(Icons.shopping_cart)),
                  const SizedBox(width: 16),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const SizedBox(height: 4),
                        Text(
                          '12/06/2020 10:00 AM',
                          style: Theme.of(context).textTheme.caption,
                        ),
                        const SizedBox(height: 8),
                        const Text(
                          'You have received an order #12345 for apple iphone7',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.black54,
                            fontSize: 15,
                          ),
                        ),
                        const SizedBox(height: 8),
                        Text(
                          'Order',
                          style:
                              Theme.of(context).textTheme.bodyMedium?.copyWith(
                                    color: colorSuccess,
                                  ),
                        ),
                      ],
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
        separatorBuilder: (context, index) => const SizedBox(height: 10),
        itemCount: 100,
      ),
    );
  }
}
