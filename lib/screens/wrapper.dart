import 'package:get/get.dart';
import 'package:flutter/material.dart';

import 'main/main.dart';

import '../controller/auth_controller.dart';
import 'auth/login.dart';

class Wrapper extends GetView<AuthController> {
  const Wrapper({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      if (controller.isLoggedIn) {
        debugPrint("User is logged in. Route: ${Get.currentRoute}");
        return const MainScreen();
      } else {
        debugPrint("User is logged out. Route: ${Get.currentRoute}");
        return const LoginScreen();
      }
    });
  }
}
