import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:get/get.dart';
import 'package:traidbiz/constraints/colors.dart';
import 'package:traidbiz/data/local/intro_db.dart';
import '/screens/intro/intro.dart';
import 'package:traidbiz/constraints/images.dart';

import '/constraints/images.dart';
import '../screens/wrapper.dart';

class Splash extends StatefulWidget {
  const Splash({Key? key}) : super(key: key);

  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
      debugPrint("First Time? :${IntroDb.isFirstTimeOpen()}");
      final _screen =
          IntroDb.isFirstTimeOpen() ? const IntroScreen() : const Wrapper();

      if (timeStamp.inSeconds < 3) {
        Future.delayed(Duration(seconds: 3 - timeStamp.inSeconds)).then((_) {
          Get.off(() => _screen);
        });
      } else {
        Get.off(() => _screen);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: colorPrimary,
      body: Center(
        child: Image.asset(splashIcon),
      ),
    );
  }
}
