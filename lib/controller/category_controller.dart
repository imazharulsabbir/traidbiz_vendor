import 'package:flutter/material.dart';
import 'package:flutter_phosphor_icons/flutter_phosphor_icons.dart';
import 'package:get/get.dart';
import 'package:traidbiz/controller/product/product_detail_controller.dart';
import '../constraints/colors.dart';
import '../data/local/auth_db.dart';
import '../data/models/product/category/category.dart';
import '../data/models/user/user.dart';
import '../utils/snackbar.dart';
import '/data/repository/product_repository.dart';

class CategoryController extends GetxController
    with StateMixin<List<ProductCategory>>, ScrollMixin {
  final ProductRepository repository;
  CategoryController(this.repository);

  ProductDetailController productDetailController =
      Get.find<ProductDetailController>();

  late AuthCookie? _authCookie;
  final _isLoading = false.obs;
  bool get isLoading => _isLoading.value;

  final _categories = <ProductCategory>[].obs;
  List<ProductCategory>? get categories => _categories;

  final int repositoriesPerPage = 100;
  int page = 1;
  bool getFirstData = false;
  bool lastPage = false;

  @override
  void onInit() {
    super.onInit();
    _authCookie = AuthDb.getAuthCookie();
    _getProductCategories();
  }

  void updateProductSelection(ProductCategory? category) {
    if (category == null) return;

    int _index = _categories.indexOf(category);
    var _data = category.copyWith(
      isSelected: category.isSelected ?? false ? false : true,
    );
    if (_index >= 0) {
      _categories.removeAt(_index);
      _categories.insert(_index, _data);
    }
    debugPrint("${category.name} is selected");

    productDetailController.categories =
        _categories.where((element) => element.isSelected == true).toList();
    update();
  }

  Future<void> _getProductCategories() async {
    await repository
        .getProductCategories(
      "${_authCookie?.user?.id}",
      perPage: repositoriesPerPage,
      pageNo: page,
    )
        .then((result) {
      final bool emptyRepositories = result.categories?.isEmpty == true;

      if (!getFirstData && emptyRepositories) {
        change(null, status: RxStatus.empty());
      } else if (getFirstData && emptyRepositories) {
        lastPage = true;
      } else {
        getFirstData = true;

        List<ProductCategory> _temp = result.categories ?? [];
        _categories.addAll(_temp);
        change(_categories, status: RxStatus.success());
      }
    }, onError: (err) {
      change(null, status: RxStatus.error(err.toString()));
    });
  }

  @override
  Future<void> onEndScroll() async {
    debugPrint('onEndScroll');
    if (!lastPage) {
      page += 1;
      Get.defaultDialog(
        onWillPop: () {
          return Future.value(false);
        },
        barrierDismissible: false,
        title: 'Loading... Please wait.',
        content: Column(
          mainAxisSize: MainAxisSize.min,
          children: const [LinearProgressIndicator(color: colorSecondary)],
        ),
      );
      await _getProductCategories();
      Get.back();
    } else {
      snack(
        'Alert',
        'All Product Loaded!',
        PhosphorIcons.warning,
      );
    }
  }

  @override
  Future<void> onTopScroll() async {
    debugPrint('onTopScroll');
  }
}
