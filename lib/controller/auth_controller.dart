import 'package:flutter/material.dart';
import 'package:dio/dio.dart' as _dio;
import 'package:flutter_phosphor_icons/flutter_phosphor_icons.dart';
import 'package:get/get.dart';
import 'package:traidbiz/utils/snackbar.dart';
import '/data/local/auth_db.dart';
import '/data/remote/api_service.dart';
import '/globals/globals.dart';
import '/screens/splash.dart';

import '../constraints/api_endpoints.dart';
import '../data/models/user/user.dart';

class AuthController extends GetxController {
  final _isLoggedIn = false.obs;
  bool get isLoggedIn => _isLoggedIn.value;

  final _isLoading = false.obs;
  bool get isLoading => _isLoading.value;

  // form keys
  late GlobalKey<FormState> authFormKey;

  // controllers
  final TextEditingController usernameController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();

  final _authCookie = const AuthCookie().obs;
  AuthCookie? get authCookie => _authCookie.value;

  final _isPasswordVisible = false.obs;
  bool get isPasswordVisible => _isPasswordVisible.value;
  void setIsPasswordVisible() {
    _isPasswordVisible.value = !_isPasswordVisible.value;
    update();
  }

  @override
  void onInit() {
    super.onInit();
    authFormKey = GlobalKey<FormState>();
    _isLoggedIn.value = AuthDb.isUserLoggedIn();
    final _cookie = AuthDb.getAuthCookie();
    if (_cookie != null) {
      _authCookie.value = _cookie;
    }
  }

  void updateProfile() {
    final _cookie = AuthDb.getAuthCookie();
    if (_cookie != null) {
      _authCookie.value = _cookie;
    }
  }

  Future<bool> login(String username, String password) async {
    final client = _dio.Dio();
    debugPrint(":: Login In ::");
    try {
      _isLoading.value = true;
      update();

      final _response = await ApiService.post(
        generateAuthCookie,
        client,
        body: {
          "username": username,
          "password": password,
        },
      );
      debugPrint("$_response");

      if (_response['status'] != 'error') {
        final authCookie = AuthCookie.fromJson(_response);
        _setAuthCookie(authCookie);
        return Future.value(true);
      } else {
        snack(
          'Oops!',
          "${_response['message']}",
          PhosphorIcons.warning,
        );
        return Future.value(false);
      }
    } catch (e) {
      return Future.value(false);
    } finally {
      _isLoading.value = false;
      update();
    }
  }

  void _setAuthCookie(AuthCookie authCookie) {
    AuthDb.setAuthCookie(authCookie);
    _authCookie.value = authCookie;

    if (authCookie.cookie != null) {
      _isLoggedIn.value = true;
    } else {
      _isLoggedIn.value = false;
    }

    update();
  }

  void logout() {
    AuthDb.clearDb();
    _isLoggedIn.value = false;
    Get.offAll(() => const Splash(), binding: GlobalBindings());
    update();
  }
}
