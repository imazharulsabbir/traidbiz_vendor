import 'package:flutter/material.dart';
import 'package:flutter_phosphor_icons/flutter_phosphor_icons.dart';
import 'package:get/get.dart';
import 'package:traidbiz/data/models/store_settings/store_settings.dart';
import '../data/repository/store_repository.dart';
import '../utils/snackbar.dart';
import '/data/local/auth_db.dart';
import '/data/models/user/user.dart';
import '/data/repository/home_repository.dart';

import '../constraints/colors.dart';
import '../data/models/order/order.dart';
import '../data/models/report/monthly_report.dart';
import '../data/repository/store_order_repository.dart';

class HomeController extends GetxController
    with StateMixin<List<StoreOrder>>, ScrollMixin {
  final HomeRepository _repository = HomeRepository();
  final StoreOrderRepository _orderRepository;

  HomeController(this._orderRepository);

  AuthCookie? authCookie;

  final RxList<MonthlyReport> _monthlyReportList = <MonthlyReport>[].obs;
  List<MonthlyReport> get monthlyReportList => _monthlyReportList;
  List<StoreOrder> _storeOrders = [];

  final int repositoriesPerPage = 20;
  int page = 1;
  bool getFirstData = false;
  bool lastPage = false;

  @override
  void onInit() {
    super.onInit();
    authCookie = AuthDb.getAuthCookie();
    getSellerDashboard();
    _getStoreOrders();
  }

  Future<void> refreshHomepage() async {
    page = 1;
    getFirstData = false;
    lastPage = false;
    getSellerDashboard();

    final StoreRepository _storeRepository = StoreRepository();
    final _storeSettings = await _storeRepository.getStoreProfileInfo();
    updateUserProfileFromStoreSettings(_storeSettings);
    await _getStoreOrders();
  }

  void updateUserProfileFromStoreSettings(StoreProfileInfo profile) {
    authCookie = authCookie?.copyWith(
      user: authCookie?.user?.copyWith(
        displayName: profile.storeProfile?.storeName,
        profileImage: profile.storeProfile?.logoUrl,
      ),
    );

    if (authCookie != null) {
      AuthDb.setAuthCookie(authCookie!);
    }
    update();
  }

  Future<void> refreshHomepageStoreOrders() async {
    page = 1;
    getFirstData = false;
    lastPage = false;
    change(null, status: RxStatus.loading());
    _storeOrders = [];
    await _getStoreOrders();
  }

  void getSellerDashboard() {
    List<MonthlyReport> _reports = [];
    _repository.getSellerDashboard().then((value) {
      _reports.add(
        MonthlyReport(
          color: colorPrimary,
          title: 'Gross Sales',
          value: value.data?.grossSales?.month,
          percentage: 10,
        ),
      );

      _reports.add(
        MonthlyReport(
          color: colorSecondary,
          title: 'Earning',
          value: value.data?.earnings?.month,
          percentage: 10,
        ),
      );

      _reports.add(
        MonthlyReport(
          color: colorSuccess,
          title: 'Sold Items',
          value: value.data?.completedOrders?.toDouble(),
          percentage: 10,
        ),
      );

      _reports.add(
        MonthlyReport(
          color: colorGrossSalesLine,
          title: 'Order Received',
          value: value.data?.totalOrders?.toDouble(),
          percentage: 10,
        ),
      );

      _monthlyReportList.value = _reports;
      update();
    }, onError: (error) {
      debugPrint("********* Error in HomeController: ${error.toString()}");
      debugPrint(error.toString());
      _reports.add(
        MonthlyReport(
          color: colorPrimary,
          title: 'Gross Sales',
          value: 0.0,
          percentage: 0,
        ),
      );

      _reports.add(
        MonthlyReport(
          color: colorSecondary,
          title: 'Earning',
          value: 0.0,
          percentage: 0,
        ),
      );

      _reports.add(
        MonthlyReport(
          color: colorSuccess,
          title: 'Sold Items',
          value: 0.0,
          percentage: 0,
        ),
      );

      _reports.add(
        MonthlyReport(
          color: colorGrossSalesLine,
          title: 'Order Received',
          value: 0.0,
          percentage: 0,
        ),
      );

      _monthlyReportList.value = _reports;
      update();
    });
  }

  Future<void> _getStoreOrders() async {
    await _orderRepository.getStoreOrders(page, repositoriesPerPage).then(
        (result) {
      final bool emptyRepositories = result.isEmpty;

      if (!getFirstData && emptyRepositories) {
        change(null, status: RxStatus.empty());
      } else if (getFirstData && emptyRepositories) {
        lastPage = true;
      } else {
        getFirstData = true;
        _storeOrders = [..._storeOrders, ...result];
        change(_storeOrders, status: RxStatus.success());
      }
    }, onError: (err) {
      change(null, status: RxStatus.error(err.toString()));
    });
  }

  @override
  Future<void> onEndScroll() async {
    debugPrint('onEndScroll');
    if (!lastPage) {
      page += 1;
      // snack('Loading', 'Loading more data. Please wait...', Icons.menu);
      Get.defaultDialog(
        onWillPop: () {
          return Future.value(false);
        },
        barrierDismissible: false,
        title: 'Loading... Please wait.',
        content: Column(
          mainAxisSize: MainAxisSize.min,
          children: const [LinearProgressIndicator(color: colorSecondary)],
        ),
      );
      await _getStoreOrders();
      Get.back();
    } else {
      snack(
        'Alert',
        'All Orders Loaded!',
        PhosphorIcons.warning,
      );
    }
  }

  @override
  Future<void> onTopScroll() async {
    debugPrint('onTopScroll');
  }
}
