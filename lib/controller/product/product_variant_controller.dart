import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:traidbiz/utils/snackbar.dart';

import '../../constraints/colors.dart';
import '../../data/models/product/available_variations/product_available_variation.dart';
import '../../data/models/product/variation/variation.dart';
import '../../data/models/product/variation_attributes/variation_attr.dart';
import '../../data/repository/product_repository.dart';
import '../../screens/product/widgets/product_variation.dart';

mixin ProductVariantController on GetxController {
  final ProductRepository _repository = Get.find<ProductRepository>();
  final GlobalKey<FormState> variationFormKey = GlobalKey<FormState>();
  final GlobalKey<FormState> variationsFormKey = GlobalKey<FormState>();

  final _creatingVariation = false.obs;
  bool get creatingVariation => _creatingVariation.value;

  final _productVariationAttributes = <ProductVariationWidget>[].obs;
  List<ProductVariationWidget> get productVariationAttributes =>
      _productVariationAttributes;

  final RxList<VariationAttribute> _availableColorVariationAttributes =
      <VariationAttribute>[].obs;
  List<VariationAttribute> get availableColorVariationAttributes =>
      _availableColorVariationAttributes;
  set availableColorVariationAttributes(List<VariationAttribute> attributes) =>
      _availableColorVariationAttributes.value = attributes;

  final RxList<VariationAttribute> _availableSizeVariationAttributes =
      <VariationAttribute>[].obs;
  List<VariationAttribute> get availableSizeVariationAttributes =>
      _availableSizeVariationAttributes;
  set availableSizeVariationAttributes(List<VariationAttribute> attributes) =>
      _availableSizeVariationAttributes.value = attributes;

  final Rx<VariationAttribute?> _selectedColorVariation =
      const VariationAttribute().obs;
  VariationAttribute? get selectedColorVariation =>
      _selectedColorVariation.value;
  set selectedColorVariation(VariationAttribute? value) =>
      _selectedColorVariation.value = value;

  final Rx<VariationAttribute?> _selectedSizeVariation =
      const VariationAttribute().obs;
  VariationAttribute? get selectedSizeVariation => _selectedSizeVariation.value;
  set selectedSizeVariation(VariationAttribute? value) =>
      _selectedSizeVariation.value = value;

  final _priceTextEditingControllers = <TextEditingController?>[].obs;
  List<TextEditingController?> get priceTextEditingControllers =>
      _priceTextEditingControllers;

  final _skuTextEditingControllers = <TextEditingController?>[].obs;
  List<TextEditingController?> get skuTextEditingControllers =>
      _skuTextEditingControllers;

  final _stockTextEditingControllers = <TextEditingController?>[].obs;
  List<TextEditingController?> get stockTextEditingControllers =>
      _stockTextEditingControllers;

  final _tags = <String>[].obs;

  @override
  void onInit() {
    super.onInit();
  }

  Future<ProductAvailableVariation> getProductAvailableVariationAttributes(
    String? productId,
  ) async {
    return await _repository.getProductAvailableVariationAttributes(productId);
  }

  void addProductVariationController(
    String tag,
    TextEditingController? priceController,
    TextEditingController? skuController,
    TextEditingController? stockController,
  ) {
    debugPrint("======= addProductVariationController ========");
    int _index = _tags.indexOf(tag);
    if (_index.isNegative) {
      _priceTextEditingControllers.add(priceController);
      _skuTextEditingControllers.add(skuController);
      _stockTextEditingControllers.add(stockController);
      _tags.add(tag);
    } else {
      _priceTextEditingControllers.removeAt(_index);
      _skuTextEditingControllers.removeAt(_index);
      _stockTextEditingControllers.removeAt(_index);

      _priceTextEditingControllers.insert(_index, priceController);
      _skuTextEditingControllers.insert(_index, skuController);
      _stockTextEditingControllers.insert(_index, stockController);
    }

    update();
  }

  void removeProductVariationController(
    String tag,
    TextEditingController? priceController,
    TextEditingController? skuController,
    TextEditingController? stockController,
  ) {
    debugPrint("======= removeProductVariationController ========");
    debugPrint("${_priceTextEditingControllers.length}");
    int _index = _tags.indexOf(tag);
    if (_index >= 0) {
      _priceTextEditingControllers.removeAt(_index);
      _skuTextEditingControllers.removeAt(_index);
      _stockTextEditingControllers.removeAt(_index);
      _tags.removeAt(_index);

      debugPrint("${_priceTextEditingControllers.length}");
      update();
    }
  }

  void setProductVariationAttribute(ProductVariationWidget widget) {
    debugPrint("======= setProductVariationAttribute ========");
    if (!_tags.contains(
      "${widget.response?.data?.variations?.attributes?.color}&${widget.response?.data?.variations?.attributes?.size}",
    )) {
      _productVariationAttributes.add(widget);
      print(_productVariationAttributes);
    } else {
      snack(
        'Success!',
        'Variation Added!',
        Icons.warning,
      );
    }
    update();
  }

  void setProductVariationAttributes(ProductVariationWidget widget) {
    _productVariationAttributes.add(widget);
    debugPrint("Adding Variation Attribute: ${widget.response}");
    update();
  }

  void clearProductVariationAttributes() {
    _productVariationAttributes.clear();
    _availableColorVariationAttributes.clear();
    _availableSizeVariationAttributes.clear();

    _tags.clear();
    _priceTextEditingControllers.clear();
    _skuTextEditingControllers.clear();
    _stockTextEditingControllers.clear();
    update();
  }

  void updateProductVariationExpanded(
    ProductVariationWidget widget,
    String tag,
  ) {
    int _index = _productVariationAttributes.indexOf(widget);

    if (_index >= 0) {
      _productVariationAttributes.removeAt(_index);
      var _temp = ProductVariationWidget(
        response: widget.response?.copyWith(
            data: widget.response?.data?.copyWith(
          variations: widget.response?.data?.variations?.copyWith(
            image: widget.response?.data?.variations?.image,
            attributes: widget.response?.data?.variations?.attributes?.copyWith(
              color: widget.response?.data?.variations?.attributes?.color,
              size: widget.response?.data?.variations?.attributes?.size,
            ),
            regularPrice: priceTextEditingControllers[_index]?.text ?? '',
            sku: skuTextEditingControllers[_index]?.text ?? '',
            stockQty: stockTextEditingControllers[_index]?.text ?? '',
          ),
        )),
        isExpanded: !widget.isExpanded,
      );
      _productVariationAttributes.insert(_index, _temp);
    }

    update();
  }

  void uploadProductVariationImage(
    ProductVariationWidget widget,
    String path,
  ) {
    int _index = _productVariationAttributes.indexOf(widget);

    if (_index >= 0) {
      var _temp = ProductVariationWidget(
        response: widget.response?.copyWith(
          data: widget.response?.data?.copyWith(
            variations: widget.response?.data?.variations?.copyWith(
              image: path,
              attributes:
                  widget.response?.data?.variations?.attributes?.copyWith(
                color: widget.response?.data?.variations?.attributes?.color,
                size: widget.response?.data?.variations?.attributes?.size,
              ),
              regularPrice: priceTextEditingControllers[_index]?.text ?? '',
              sku: skuTextEditingControllers[_index]?.text ?? '',
              stockQty: stockTextEditingControllers[_index]?.text ?? '',
            ),
          ),
        ),
        isExpanded: widget.isExpanded,
      );

      _productVariationAttributes.removeAt(_index);
      _productVariationAttributes.insert(_index, _temp);
    }

    update();
  }

  void removeProductVariationAttribute(ProductVariationWidget widget) async {
    int _index = _productVariationAttributes.indexOf(widget);

    try {
      _creatingVariation.value = true;
      update();
      final _response = await _repository.removeProductVariations(
        widget.response?.data?.variations?.variationId?.toString(),
      );
      snack('Remove Variation', '${_response?['message']}', Icons.message);
      if (_index >= 0) {
        int _tagIndex = _tags.indexOf(widget.tag);

        try {
          if (!_tagIndex.isNegative) {
            _priceTextEditingControllers.removeAt(_tagIndex);
            _skuTextEditingControllers.removeAt(_tagIndex);
            _stockTextEditingControllers.removeAt(_tagIndex);
          }
        } catch (e) {
          debugPrint("**** Removing text controller error. ${e.toString()}");
        }

        _tags.remove(widget.tag);
        _productVariationAttributes.removeAt(_index);
      }
      update();
    } catch (e) {
      snack('Remove Variation Failed!', '$e', Icons.message);
      return Future.error('$e');
    } finally {
      _creatingVariation.value = false;
      update();
    }
  }

  Future<ProductVariationResponse?> createProductVariation({
    String? productId,
    String? regularPrice,
    String? color,
    String? size,
  }) async {
    try {
      Get.defaultDialog(
        onWillPop: () {
          return Future.value(false);
        },
        barrierDismissible: false,
        title: 'Create Variation',
        content: Column(
          mainAxisSize: MainAxisSize.min,
          children: const [LinearProgressIndicator(color: colorSecondary)],
        ),
      );

      final _response = await _repository.createProductVariation(
        productId: productId,
        regularPrice: regularPrice,
        color: color,
        size: size,
      );

      Get.back();
      snack('Variation', '${_response?.message}', Icons.message);

      if (_response?.status == 'success') {
        setProductVariationAttribute(
          ProductVariationWidget(response: _response),
        );
      }

      debugPrint("Response:: $_response");
      return _response;
    } catch (e) {
      Get.back();
      snack(
        'Add Variation',
        'Failed to add variation! Try again.',
        Icons.error,
      );
      return Future.error('$e');
    }
  }

  void updateProductVariations(
    String productId,
    ProductVariationWidget? widget,
  ) async {
    try {
      final _attributes = widget?.response?.data?.variations?.attributes;
      final _tag = "${_attributes?.color}&${_attributes?.size}";
      int _tagIndex = _tags.indexOf(_tag);

      if (_tagIndex.isNegative) {
        snack(
          'Failed!',
          'There was something unexpected happened! Please go back and try again.',
          Icons.restore,
        );
        return;
      }

      Get.defaultDialog(
        onWillPop: () {
          return Future.value(false);
        },
        barrierDismissible: false,
        title: 'Update Variation',
        content: Column(
          mainAxisSize: MainAxisSize.min,
          children: const [LinearProgressIndicator(color: colorSecondary)],
        ),
      );

      final _attributeData = widget?.response;
      final _requestBody = _attributeData?.copyWith(
        data: _attributeData.data?.copyWith(
          variations: _attributeData.data?.variations?.copyWith(
            regularPrice: priceTextEditingControllers[_tagIndex]?.text,
            sku: skuTextEditingControllers[_tagIndex]?.text,
            stockQty: stockTextEditingControllers[_tagIndex]?.text,
          ),
        ),
      );

      final item = ProductVariationWidget(
        response: _requestBody,
      );

      final _response = await _repository.updateProductVariations(
        productId,
        item,
      );
      Get.back();
      snack('Success!', '$_response', Icons.done);
    } catch (e) {
      debugPrint("**** Update Variation error. ${e.toString()}");
      Get.back();
      snack('Failed!', '$e', Icons.error);
    }
  }
}
