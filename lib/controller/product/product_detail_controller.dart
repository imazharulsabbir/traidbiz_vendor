import 'dart:io';

import 'package:flutter/material.dart';
import 'dart:convert' as convert;
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:traidbiz/data/models/product/variation/variation.dart';
import '../../constraints/colors.dart';
import '../../data/models/product/product_model.dart';
import '../../screens/product/create_product.dart';
import '../../screens/product/widgets/product_attribute.dart';
import '../../screens/product/widgets/product_variation.dart';
import '/controller/product/product_attribute_controller.dart';
import '/controller/product/product_shipping_controller.dart';
import '/controller/product/product_tax_controller.dart';
import '/controller/product/product_variant_controller.dart';
import '/screens/product/update_product.dart';
import '/utils/snackbar.dart';
import '../../data/models/product/create/create_product.dart';
import '/data/local/auth_db.dart';
import '/data/models/user/user.dart';
import '/data/repository/product_repository.dart';

import '../../data/models/product/category/category.dart';
import 'product_controller.dart';

class ProductDetailController extends GetxController
    with
        ProductShippingController,
        ProductAttributeController,
        ProductVariantController,
        ProductTaxController {
  late AuthCookie? _authCookie;
  final _isLoading = false.obs;
  bool get isLoading => _isLoading.value;

  final _isProductCreating = false.obs;
  bool get isProductCreating => _isProductCreating.value;

  final _isProductUpdating = false.obs;
  bool get isProductUpdating => _isProductUpdating.value;

  final _productType = "".obs;
  String? get productType =>
      _productType.value.isEmpty ? null : _productType.value;
  set productType(String? type) => _productType.value = type ?? "";

  final ProductRepository _repository = ProductRepository();
  final _categories = <ProductCategory>[].obs;
  List<ProductCategory>? get categories => _categories;
  set categories(List<ProductCategory>? value) =>
      _categories.value = value ?? [];

  // form keys
  final GlobalKey<FormState> productFormKey = GlobalKey<FormState>();
  final GlobalKey<FormState> updateProductFormKey = GlobalKey<FormState>();

  // controllers
  final TextEditingController productNameController = TextEditingController();
  final TextEditingController regularPriceController = TextEditingController();
  final TextEditingController salePriceController = TextEditingController();
  final TextEditingController skuController = TextEditingController();

  final ImagePicker _picker = ImagePicker();
  final _productImage = ''.obs;
  String get productImage => _productImage.value;
  set productImage(String image) => _productImage.value = image;

  final _productGalleryImages = <String>[].obs;
  List<String> get productGalleryImages => _productGalleryImages;

  final _createdProductId = "".obs;
  String get createdProductId => _createdProductId.value;
  set createdProductId(String id) => _createdProductId.value = id;

  @override
  void onInit() {
    super.onInit();
    _authCookie = AuthDb.getAuthCookie();
  }

  void setProductImage(String path) {
    _productImage.value = path;
    update();
  }

  void setProductGalleryImage(int index, String path) {
    if (_productGalleryImages.length - 1 >= index) {
      _productGalleryImages[index] = path;
    } else {
      _productGalleryImages.add(path);
    }
    update();
  }

  Future<XFile?> pickImage({ImageSource? source}) async {
    try {
      final _image = await _picker.pickImage(
        source: source ?? ImageSource.gallery,
      );
      return Future.value(_image);
    } catch (e) {
      snack('Failed to pick image!', '$e', Icons.error);
      return null;
    }
  }

  void pickProductGalleryImage(ImageSource source, int index) {
    _picker.pickImage(source: source).then((file) {
      if (file != null) {
        _productGalleryImages.add(file.path);
        update();
      }
    });
  }

  void createNewProduct() async {
    // return Get.off(() => const UpdateProductScreen());

    if (productFormKey.currentState?.validate() == true) {
      if (productImage.isEmpty) {
        snack('Please select product image!', '', Icons.error);
        return;
      }

      if (productGalleryImages.isEmpty) {
        snack('Please select product gallery images!', '', Icons.error);
        return;
      }
      final _categories = categories
          ?.where((element) => element.isSelected == true)
          .map((e) => e.id)
          .toList()
          .join(",");
      if (_categories?.isEmpty == true) {
        snack(
          'Category Required!',
          'Please select product category!',
          Icons.error,
        );
        return;
      }

      _isProductCreating.value = true;
      update();
      try {
        debugPrint("Categories: $_categories");
        _authCookie = AuthDb.getAuthCookie();

        final _body = CreateProductModel(
          cookie: _authCookie?.cookie,
          productName: productNameController.text,
          productPrice: salePriceController.text,
          productType: 'simple',
          productDescription: "",
          categories: _categories,
          sku: skuController.text,
          productImage: convert.base64Encode(
            File(productImage).readAsBytesSync(),
          ),
          productGallery: _productGalleryImages
              .map((element) => ProductGallery(
                    base64Img: convert.base64Encode(
                      File(element).readAsBytesSync(),
                    ),
                  ))
              .toList(),
        );

        debugPrint(_body.toString());

        final _response = await _repository.createNewProduct(_body);
        if (_response['status'] == 'success') {
          _createdProductId.value = _response['response']['id'].toString();
          _productType.value = 'simple';
          debugPrint("Product created! Id: $_createdProductId");
          // replace with update product screen

          Get.back();
          final _productController = Get.find<ProductController>();
          _productController.refreshProducts();
        }
        snack('Message', _response['message'], Icons.message);
        // debugPrint("$_response");
      } catch (e) {
        debugPrint("$e");
      } finally {
        _isProductCreating.value = false;
        update();

        final _productController = Get.find<ProductController>();
        _productController.refreshProducts();
      }
    }
  }

  Future<dynamic> updateProduct() async {
    if (updateProductFormKey.currentState?.validate() == true) {
      final _categories = categories
          ?.where((element) => element.isSelected == true)
          .map((e) => e.id)
          .toList()
          .join(",");
      if (_categories?.isEmpty == true) {
        snack(
          'Category Required!',
          'Please select product category!',
          Icons.error,
        );
        return false;
      }

      if (productImage.isEmpty) {
        snack('Please select product image!', '', Icons.error);
        return;
      }

      _isProductUpdating.value = true;
      update();

      try {
        final _body = {
          "product_id": createdProductId,
          "product_name": productNameController.text,
          "product_type": productType?.toLowerCase(),
          "product_price": regularPriceController.text,
          "product_description": "",
          "categories": _categories,
          "shipping_class_id": selectedShippingClass.termId,
          "tax_status": selectedTaxStatus,
          "tax_class": selectedTaxClass
        };

        if (!productImage.contains("http")) {
          _body['image'] = convert.base64Encode(
            File(productImage).readAsBytesSync(),
          );
        }

        debugPrint(_body.toString());

        final _response = await _repository.updateProductInfo(_body);
        return _response;
      } catch (e) {
        snack('Error', '$e', Icons.error);
        return Future.error(e);
      } finally {
        _isProductUpdating.value = false;
        update();
      }
    }
  }

  void gotoCreateProduct() {
    productNameController.text = "";
    regularPriceController.text = "";
    salePriceController.text = "";
    skuController.text = "";
    _productImage.value = "";
    _productGalleryImages.clear();
    _categories.value = [];

    categories = categories
        ?.map((element) => element.copyWith(
              isSelected: false,
            ))
        .toList();
    update();

    Get.to(() => const CreateProductScreen());
  }

  void gotoUpdateProductScreen(
    ProductModel? product,
  ) async {
    if (product?.id == null) return;

    Get.defaultDialog(
        onWillPop: () {
          return Future.value(false);
        },
        barrierDismissible: false,
        title: 'Loading... Please wait.',
        content: Column(
          mainAxisSize: MainAxisSize.min,
          children: const [LinearProgressIndicator(color: colorSecondary)],
        ),
      );
    _createdProductId.value = "${product?.id}";
    productNameController.text = "${product?.title}";
    regularPriceController.text = "${product?.price}";
    _productType.value = product?.type ?? 'simple';
    productImage = product?.imageUrl ?? "";

    clearProductAttribute();
    clearProductVariationAttributes();

    categories = product?.categories
        ?.map((e) => e.copyWith(
              isSelected: true,
            ))
        .toList();

    for (var element in shippingClasses) {
      if (element.name?.contains(product?.shippingClass ?? '', 0) == true) {
        selectedShippingClass = element;
      }
    }

    for (var element in taxStatus) {
      if (element.contains("${product?.taxStatus}", 0)) {
        selectedTaxStatus = element;
      }
    }

    for (var element in taxClass) {
      if (element.contains("${product?.taxClass}", 0)) {
        selectedTaxClass = element;
      }
    }

    // product available attributes
    if (productType != "simple") {
      try {
        final _availableAttributes = await _repository.getProductAttributes(
          product?.id.toString(),
        );
        if (_availableAttributes?.data != null) {
          final _colors = _availableAttributes?.data?.colors;
          final _sizes = _availableAttributes?.data?.sizes;

          if (_colors != null && _colors.isNotEmpty) {
            setProductAttribute(const ProductAttributeWidget(
              name: 'Color',
              isExpanded: true,
            ));
            setAvailableColors(_colors);
          }

          if (_sizes != null && _sizes.isNotEmpty) {
            setProductAttribute(const ProductAttributeWidget(
              name: 'Size',
              isExpanded: true,
            ));
            setAvailableSizes(_sizes);
          }
        } else {
          debugPrint("There is no available product attributes.");
        }
        showAvailableProductAttributes();
      } catch (e) {
        debugPrint("$e");
        Get.back();

        snack('Warning!', '$e', Icons.warning);
        return;
      }

      // product available variations
      try {
        final _response = await getProductAvailableVariationAttributes(
          product?.id.toString(),
        );

        _response.data?.forEach((element) {
          debugPrint(element.toString());

          setProductVariationAttributes(
            ProductVariationWidget(
              response: ProductVariationResponse(
                data: ProductVariationBody(
                  productId: "${product?.id}",
                  variations: ProductVariationData(
                    variationId: element.variationId,
                    attributes: ProductVariationAttributeData(
                      color: element.attribute?.color,
                      size: element.attribute?.size,
                    ),
                    regularPrice: element.regularPrice,
                    stockQty: element.quantity,
                    sku: element.sku,
                    image: element.image?.src,
                  ),
                ),
              ),
            ),
          );
        });
      } catch (e) {
        debugPrint("$e");
        Get.back();

        snack('Warning!', '$e', Icons.warning);
        return;
      }
    }
    update();

    Get.off(() => const UpdateProductScreen());
  }

  Future<dynamic> removeProduct(int? productId) async {
    try {
      Get.defaultDialog(
        onWillPop: () {
          return Future.value(false);
        },
        barrierDismissible: false,
        title: 'Loading... Please wait.',
        content: Column(
          mainAxisSize: MainAxisSize.min,
          children: const [LinearProgressIndicator(color: colorSecondary)],
        ),
      );
      final _response = await _repository.removeProduct(productId);
      // debugPrint(_response.toString());
      Get.back(); // close loading dialog
      snack('Message', "${_response['message']}.", Icons.message);
      final _productController = Get.find<ProductController>();
      _productController.refreshProducts();
    } catch (e) {
      Get.back(); // close loading dialog
    }
  }
}
